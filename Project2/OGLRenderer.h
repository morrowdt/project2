#pragma once
#ifndef OGL_SHADER
#define OGL_SHADER

#include "Renderer.h"
#include "OGLObject.h"
#include <map>
using std::map;

#include <gl/glew.h>

class Logger;
class OGLShaderCompiler;
class OGLShaderProgram;

class OGLRenderer : public Renderer
{
protected:
	struct Vertex {
		GLfloat x, y, z;
		GLfloat red, green, blue;
	}trianglesVertexData[6], linesVertexData[4], pointsVertexData[4];

	OGLShaderCompiler * vertexShader;
	OGLShaderCompiler * fragmentShader;
	OGLShaderProgram * shaderProgram;

	map<string, OGLObject*> objects;

public:
	OGLRenderer(OGLShaderCompiler * vertexShader, OGLShaderCompiler * fragmentShader, OGLShaderProgram * shaderProgram);
	~OGLRenderer(void);

	bool create();
	void renderObjects();

protected:
	bool setupShaders();

	void createStockObject();
	void createSceneObject();
};

#endif

