#include "GameWindow.h"
#include "OGLRenderer.h"

#include <GL\glew.h>

GameWindow *GameWindow::self;

GameWindow::GameWindow(Renderer* renderer, wstring title, int width, int height):
	Win32OGLWindow(title, width, height)
{
	this->renderer = renderer;
	this->WndProcedure = GameWindow::WndProc;
	this->self = this;
	this->state = 0;
	this->timer = 0;
}

GameWindow::~GameWindow()
{
}

void GameWindow::runOneFrame()
{
	update();
	render();
}

void GameWindow::update()
{
	if (this->state == 0)
	{
		this->background.red += 0.002;
		this->background.green += 0.001;
		if (this->background.red >= 1.0f)
		{
			this->state = 1;
		}
	}
	if (this->state == 1)
	{
		this->background.red -= 0.004;
		this->background.green += 0.004;
		this->background.blue += 0.008;
		if (this->background.blue >= 1.0f)
		{
			this->state = 2;
		}
	}
	if (this->state == 2)
	{
		this->timer += 1;
		if (this->timer >= 200)
		{
			this->timer = 0;
			this->state = 3;
		}
	}
	if (this->state == 3)
	{
		this->background.red += 0.004;
		this->background.green -= 0.004;
		this->background.blue -= 0.008;
		if (this->background.red >= 1.0f)
		{
			this->state = 4;
		}
	}
	if (this->state == 4)
	{
		this->background.red -= 0.002;
		this->background.green -= 0.001;
		if (this->background.green <= 0.0f)
		{
			this->state = 5;
		}
	}
	if (this->state == 5)
	{
		this->timer += 1;
		if (this->timer >= 100)
		{
			this->timer = 0;
			this->state = 0;
		}
	}
	//this->background.green = 0.25f;
}

bool GameWindow::createRenderer()
{
	if(!this->renderer->create()) return false;
	return true;
}

void GameWindow::render()
{
	glViewport(0, 0, this->width, this->height);

	glClearColor(
		this->background.red,
		this->background.green,
		this->background.blue,
		this->background.alpha
	);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	this->renderer->renderObjects();

	SwapBuffers(this->deviceContext);
}

LRESULT CALLBACK GameWindow::WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	return Win32OGLWindow::WndProc(hWnd, message, wParam, lParam);
}
