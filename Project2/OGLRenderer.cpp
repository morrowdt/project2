#include "OGLRenderer.h"
#include "WindowsConsoleLogger.h"
#include "OGLShaderCompiler.h"
#include "OGLShaderProgram.h"

#include <cstdlib>

#include <iostream>
#include <fstream>

#include <string>
using std::string;
using namespace std;

OGLRenderer::OGLRenderer(
	OGLShaderCompiler * vertexShader,
	OGLShaderCompiler * fragmentShader,
	OGLShaderProgram * shaderProgram)
{
	this->vertexShader = vertexShader;
	this->fragmentShader = fragmentShader;
	this->shaderProgram = shaderProgram;

	this->objects["scene1"] = new OGLObject("scene1");
	this->objects["scene2"] = new OGLObject("scene2");

}

OGLRenderer::~OGLRenderer(void)
{
	for (auto iterator = objects.begin(); iterator != objects.end(); iterator++)
	{
		delete iterator->second;
	}
}

bool OGLRenderer::create()
{
	if (this->setupShaders()) {
		//this->createStockObject();
		this->createSceneObject();
		return true;
	}
	return false;
}

void OGLRenderer::renderObjects()
{
	for (auto iterator = objects.begin(); iterator != objects.end(); iterator++)
	{
		iterator->second->render(this->shaderProgram->getHandle());
	}
}

bool OGLRenderer::setupShaders()
{
	bool result = false;
	result = this->vertexShader->compile();
	if (!result) return false;

	result = this->fragmentShader->compile();
	if (!result) return false;

	return this->shaderProgram->link(
		this->vertexShader->getHandle(), 
		this->fragmentShader->getHandle()
		);
}

void OGLRenderer::createStockObject()
{
	// Points ///////////////////////////////////////////////////////////
	// Point 1
	pointsVertexData[0].x = -0.1f;
	pointsVertexData[0].y = 0.1f;
	pointsVertexData[0].z = 0.0f;
	pointsVertexData[0].red = 1.0f;
	pointsVertexData[0].green = 0.0f;
	pointsVertexData[0].blue = 0.0f;
	// Point 2
	pointsVertexData[1].x = -0.1f;
	pointsVertexData[1].y = -0.1f;
	pointsVertexData[1].z = 0.0f;
	pointsVertexData[1].red = 0.0f;
	pointsVertexData[1].green = 1.0f;
	pointsVertexData[1].blue = 0.0f;
	// Point 3
	pointsVertexData[2].x = 0.1f;
	pointsVertexData[2].y = -0.1f;
	pointsVertexData[2].z = 0.0f;
	pointsVertexData[2].red = 0.0f;
	pointsVertexData[2].green = 0.0f;
	pointsVertexData[2].blue = 1.0f;
	// Point 4
	pointsVertexData[3].x = 0.1f;
	pointsVertexData[3].y = 0.1f;
	pointsVertexData[3].z = 0.0f;
	pointsVertexData[3].red = 0.0f;
	pointsVertexData[3].green = 1.0f;
	pointsVertexData[3].blue = 1.0f;

	VBOObject * points = OGLObject::createVBOObject("points");
	points->buffer = pointsVertexData;
	points->primitiveType = GL_POINTS;
	points->bufferSizeInBytes = sizeof(pointsVertexData);
	points->numberOfVertices = 4;
	points->positionComponent.count = 3;
	points->positionComponent.type = GL_FLOAT;
	points->positionComponent.bytesToFirst = 0;
	points->positionComponent.bytesToNext = sizeof(Vertex);
	points->colorComponent.count = 3;
	points->colorComponent.type = GL_FLOAT;
	points->colorComponent.bytesToFirst = sizeof(GLfloat) * 3;
	points->colorComponent.bytesToNext = sizeof(Vertex);
	this->objects["PLT"]->addVBOObject(points);

	// Lines ////////////////////////////////////////////////////////////
	// X axis line
	linesVertexData[0].x = -1.0f;
	linesVertexData[0].y = 0.0f;
	linesVertexData[0].z = 0.0f;
	linesVertexData[0].red = 1.0f;
	linesVertexData[0].green = 0.0f;
	linesVertexData[0].blue = 0.0f;
	linesVertexData[1].x = 1.0f;
	linesVertexData[1].y = 0.0f;
	linesVertexData[1].z = 0.0f;
	linesVertexData[1].red = 1.0f;
	linesVertexData[1].green = 0.0f;
	linesVertexData[1].blue = 0.0f;
	// Y axis line
	linesVertexData[2].x = 0.0f;
	linesVertexData[2].y = 1.0f;
	linesVertexData[2].z = 0.0f;
	linesVertexData[2].red = 0.0f;
	linesVertexData[2].green = 1.0f;
	linesVertexData[2].blue = 0.0f;
	linesVertexData[3].x = 0.0f;
	linesVertexData[3].y = -1.0f;
	linesVertexData[3].z = 0.0f;
	linesVertexData[3].red = 0.0f;
	linesVertexData[3].green = 1.0f;
	linesVertexData[3].blue = 0.0f;

	VBOObject * lines = OGLObject::createVBOObject("lines");
	lines->buffer = linesVertexData;
	lines->primitiveType = GL_LINES;
	lines->bufferSizeInBytes = sizeof(linesVertexData);
	lines->numberOfVertices = 4;
	lines->positionComponent.count = 3;
	lines->positionComponent.type = GL_FLOAT;
	lines->positionComponent.bytesToFirst = 0;
	lines->positionComponent.bytesToNext = sizeof(Vertex);
	lines->colorComponent.count = 3;
	lines->colorComponent.type = GL_FLOAT;
	lines->colorComponent.bytesToFirst = sizeof(GLfloat) * 3;
	lines->colorComponent.bytesToNext = sizeof(Vertex);
	this->objects["PLT"]->addVBOObject(lines);

	// Triangles ////////////////////////////////////////////////////////
	// First triangle
	trianglesVertexData[0].x = -0.3f;
	trianglesVertexData[0].y = 0.0f;
	trianglesVertexData[0].z = 0.0f;
	trianglesVertexData[0].red = 1.0f;
	trianglesVertexData[0].green = 0.0f;
	trianglesVertexData[0].blue = 0.0f;
	trianglesVertexData[1].x = -0.4f;
	trianglesVertexData[1].y = 0.5f;
	trianglesVertexData[1].z = 0.0f;
	trianglesVertexData[1].red = 0.0f;
	trianglesVertexData[1].green = 0.0f;
	trianglesVertexData[1].blue = 1.0f;
	trianglesVertexData[2].x = -0.5f;
	trianglesVertexData[2].y = 0.0f;
	trianglesVertexData[2].z = 0.0f;
	trianglesVertexData[2].red = 0.0f;
	trianglesVertexData[2].green = 1.0f;
	trianglesVertexData[2].blue = 0.0f;
	// Second triangle
	trianglesVertexData[3].x = 0.4f;
	trianglesVertexData[3].y = 0.5f;
	trianglesVertexData[3].z = 0.0f;
	trianglesVertexData[3].red = 0.0f;
	trianglesVertexData[3].green = 1.0f;
	trianglesVertexData[3].blue = 0.0f;
	trianglesVertexData[4].x = 0.5f;
	trianglesVertexData[4].y = 0.0f;
	trianglesVertexData[4].z = 0.0f;
	trianglesVertexData[4].red = 0.0f;
	trianglesVertexData[4].green = 0.0f;
	trianglesVertexData[4].blue = 1.0f;
	trianglesVertexData[5].x = 0.6f;
	trianglesVertexData[5].y = 0.5f;
	trianglesVertexData[5].z = 0.0f;
	trianglesVertexData[5].red = 0.0f;
	trianglesVertexData[5].green = 1.0f;
	trianglesVertexData[5].blue = 1.0f;

	VBOObject * triangles = OGLObject::createVBOObject("triangles");
	triangles->buffer = trianglesVertexData;
	triangles->primitiveType = GL_TRIANGLES;
	triangles->bufferSizeInBytes = sizeof(trianglesVertexData);
	triangles->numberOfVertices = 6;
	triangles->positionComponent.count = 3;
	triangles->positionComponent.type = GL_FLOAT;
	triangles->positionComponent.bytesToFirst = 0;
	triangles->positionComponent.bytesToNext = sizeof(Vertex);
	triangles->colorComponent.count = 3;
	triangles->colorComponent.type = GL_FLOAT;
	triangles->colorComponent.bytesToFirst = sizeof(GLfloat) * 3;
	triangles->colorComponent.bytesToNext = sizeof(Vertex);
	this->objects["PLT"]->addVBOObject(triangles);
}

void OGLRenderer::createSceneObject()
{
	string sceneFile = "scene.txt";
	string sceneData;
	ifstream fin;			// Input file stream
	
	fin.open(sceneFile);
	if (fin.fail())
	{
		cout << "\nThere was error opening the file." << endl;
		exit(1);
	}

	//getline(fin, sceneData);
	//int numberOfObjects = stoi(sceneData);

	Vertex triangles[102];
	Vertex lines[6];

	int triangleIndex = 0;
	int lineIndex = 0;

	string objectData;

	while (!fin.eof())
	{
		getline(fin, sceneData);
		if (sceneData.find("triangle:") != string::npos)			// If the line is a title, set the title
		{
			objectData = sceneData.substr(9, string::npos);
			char* pch = strtok((char*)objectData.c_str(), ",");

			triangles[triangleIndex].x = stof(pch);
			pch = strtok(NULL, ",");

			triangles[triangleIndex].y = stof(pch);
			pch = strtok(NULL, ",");

			triangles[triangleIndex].z = stof(pch);
			pch = strtok(NULL, ",");

			triangles[triangleIndex].red = stof(pch);
			pch = strtok(NULL, ",");

			triangles[triangleIndex].green = stof(pch);
			pch = strtok(NULL, ",");

			triangles[triangleIndex].blue = stof(pch);
			pch = strtok(NULL, ",");

			triangleIndex++;

			triangles[triangleIndex].x = stof(pch);
			pch = strtok(NULL, ",");

			triangles[triangleIndex].y = stof(pch);
			pch = strtok(NULL, ",");

			triangles[triangleIndex].z = stof(pch);
			pch = strtok(NULL, ",");

			triangles[triangleIndex].red = stof(pch);
			pch = strtok(NULL, ",");

			triangles[triangleIndex].green = stof(pch);
			pch = strtok(NULL, ",");

			triangles[triangleIndex].blue = stof(pch);
			pch = strtok(NULL, ",");

			triangleIndex++;

			triangles[triangleIndex].x = stof(pch);
			pch = strtok(NULL, ",");

			triangles[triangleIndex].y = stof(pch);
			pch = strtok(NULL, ",");

			triangles[triangleIndex].z = stof(pch);
			pch = strtok(NULL, ",");

			triangles[triangleIndex].red = stof(pch);
			pch = strtok(NULL, ",");

			triangles[triangleIndex].green = stof(pch);
			pch = strtok(NULL, ",");

			triangles[triangleIndex].blue = stof(pch);
			pch = strtok(NULL, ",");

			triangleIndex++;
		}
		if (sceneData.find("line:") != string::npos)
		{
			objectData = sceneData.substr(5, string::npos);
			char* pch = strtok((char*)objectData.c_str(), ",");

			lines[lineIndex].x = stof(pch);
			pch = strtok(NULL, ",");

			lines[lineIndex].y = stof(pch);
			pch = strtok(NULL, ",");

			lines[lineIndex].z = stof(pch);
			pch = strtok(NULL, ",");

			lines[lineIndex].red = stof(pch);
			pch = strtok(NULL, ",");

			lines[lineIndex].green = stof(pch);
			pch = strtok(NULL, ",");

			lines[lineIndex].blue = stof(pch);
			pch = strtok(NULL, ",");

			lineIndex++;

			lines[lineIndex].x = stof(pch);
			pch = strtok(NULL, ",");

			lines[lineIndex].y = stof(pch);
			pch = strtok(NULL, ",");

			lines[lineIndex].z = stof(pch);
			pch = strtok(NULL, ",");

			lines[lineIndex].red = stof(pch);
			pch = strtok(NULL, ",");

			lines[lineIndex].green = stof(pch);
			pch = strtok(NULL, ",");

			lines[lineIndex].blue = stof(pch);
			pch = strtok(NULL, ",");

			lineIndex++;
		}
	}

	VBOObject * trianglesScene = OGLObject::createVBOObject("trianglesScene");
	trianglesScene->buffer = triangles;
	trianglesScene->primitiveType = GL_TRIANGLES;
	trianglesScene->bufferSizeInBytes = sizeof(triangles);
	trianglesScene->numberOfVertices = 102;
	trianglesScene->positionComponent.count = 3;
	trianglesScene->positionComponent.type = GL_FLOAT;
	trianglesScene->positionComponent.bytesToFirst = 0;
	trianglesScene->positionComponent.bytesToNext = sizeof(Vertex);
	trianglesScene->colorComponent.count = 3;
	trianglesScene->colorComponent.type = GL_FLOAT;
	trianglesScene->colorComponent.bytesToFirst = sizeof(GLfloat) * 3;
	trianglesScene->colorComponent.bytesToNext = sizeof(Vertex);
	this->objects["scene1"]->addVBOObject(trianglesScene);

	VBOObject * linesScene = OGLObject::createVBOObject("linesScene");
	linesScene->buffer = lines;
	linesScene->primitiveType = GL_LINES;
	linesScene->bufferSizeInBytes = sizeof(lines);
	linesScene->numberOfVertices = 6;
	linesScene->positionComponent.count = 3;
	linesScene->positionComponent.type = GL_FLOAT;
	linesScene->positionComponent.bytesToFirst = 0;
	linesScene->positionComponent.bytesToNext = sizeof(Vertex);
	linesScene->colorComponent.count = 3;
	linesScene->colorComponent.type = GL_FLOAT;
	linesScene->colorComponent.bytesToFirst = sizeof(GLfloat) * 3;
	linesScene->colorComponent.bytesToNext = sizeof(Vertex);
	this->objects["scene2"]->addVBOObject(linesScene);
}